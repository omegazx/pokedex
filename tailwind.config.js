const plugin = require('tailwindcss/plugin')

module.exports = {
  purge: [],
  theme: {
    extend: {
      height: {
        '15': '3.75rem',
      },
      fontSize: {
        '2xl': '1.375rem',
        '3xl': '1.625rem'
      },
      colors: {
        'gold': '#ECA539',
        'dark-red': '#C00E20',
        'red': '#F22539',
        'gray-darker': '#353535',
        'gray-dark': '#5E5E5E',
        'gray': '#BFBFBF',
        'gray-light': '#E8E8E8',
        'gray-lighter': '#F5F5F5',
        'gray-lightest': '#F9F9F9',
      }
    }
  },
  variants: {
    extend: {
      backgroundColor: ['active']
    }
  }
}
