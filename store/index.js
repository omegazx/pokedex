export const state = () => ({
  showFavorites: false,
  favoriteList: [],
  selectedPokemon: null,
  showModal: false
})

export const getters = {
  getShowFavorites: (state) => {
    return state.showFavorites
  },
  getFavoriteList: (state) => {
    return state.favoriteList
  },
  getSelectedPokemon: (state) => {
    return state.selectedPokemon
  },
  getShowModal: (state) => {
    return state.showModal
  }
}

export const mutations = {
  SET_SHOW_FAVORITES (state, value) {
    state.showFavorites = value
  },
  ADD_TO_FAVORITE_LIST (state, value) {
    if (state.favoriteList.indexOf(value) === -1){
      state.favoriteList.push(value)
    }
  },
  REMOVE_FROM_FAVORITE_LIST (state, value) {
    state.favoriteList = state.favoriteList.filter(el => el !== value)
  },
  SET_SELECTED_POKEMON (state, value) {
    state.selectedPokemon = value
  },
  SET_SHOW_MODAL (state, value) {
    state.showModal = value
  }
}

export const actions = {
  async nuxtServerInit ({ dispatch }) {
    // dispatch('')
  }
}